import React, { useState } from 'react';
import classNames from 'classnames';
import { SectionProps } from '../utils/SectionProps';
import ButtonGroup from '../components/elements/ButtonGroup';
import Button from '../components/elements/Button';
import Image from '../components/elements/Image';
import Modal from '../components/elements/Modal';
import firebase from '../firebase/config';
import { useFormik } from 'formik';
import axios from 'axios';

import { css } from "@emotion/core";
import { useForm } from "react-hook-form";

import ContactForm from '../Form/ContactForm'

const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}

let contact = {
  name: '',
  email: '',
  phone: '',
  more: ''
}

const Event = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  ...props
}) => {

  const [videoModalActive, setVideomodalactive] = useState(false);



  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [phone, setPhone] = useState("");
  const [more, setMore] = useState("");
  const [subject, setSubject] = useState('');
  const [loading, setLoading] = useState(false);
  const [contact,setContact] = useState(null);


  let [color, setColor] = useState("#01A982");
  const openModal = (e) => {
    e.preventDefault();
    setVideomodalactive(true);
  }

  const closeModal = (e) => {
    e.preventDefault();
    setVideomodalactive(false);
  }

  const outerClasses = classNames(
    'hero section center-content',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'hero-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );





  //const db = firebase.firestore();
  const signup = async () => {
    setLoading(true);
    axios.post(localStorage.getItem('url') + '/contact', {
      name: name,
      email: email,
      phone: phone,
      subject: subject,
      more: more
    })
      .then(function (response) {
        setLoading(false);
        console.log(response);
        setName(''); setEmail(''); setPhone(''); setMore(''); setSubject('');
        alert("Your details got saved ! We will get back to you soon ");
      }, (error) => {
        console.log('from error')
        console.log(error);
        setLoading(false);
      })

  }

  
  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container-xl">
        <div className={innerClasses}>
          <div className="hero-content">
            <h1 className="mt-0 mb-16 reveal-from-bottom" data-reveal-delay="200">
              Appreciate for your <span className="text-color-primary">Contact</span>
            </h1>
            <div className="container-xl">
              <p className="m-0 mb-32 reveal-from-bottom" data-reveal-delay="400">
                please submit your contact below
              </p>

              <ContactForm sendToParent={setContact}/>


             
            </div>
          </div>
          <div className="hero-figure reveal-from-bottom illustration-element-01" data-reveal-value="20px" data-reveal-delay="800">
            <a
              data-video="https://player.vimeo.com/video/174002812"
              href="#0"
              aria-controls="video-modal"
              onClick={openModal}
            >

            </a>
          </div>
          <Modal
            id="video-modal"
            show={videoModalActive}
            handleClose={closeModal}
            video="https://player.vimeo.com/video/174002812"
            videoTag="iframe" />
        </div>
      </div>
    </section>
  );
}

Event.propTypes = propTypes;
Event.defaultProps = defaultProps;

export default Event;