import React, { useState } from 'react';
import classNames from 'classnames';
import { SectionProps } from '../../utils/SectionProps';
import ButtonGroup from '../elements/ButtonGroup';
import Button from '../elements/Button';
import Image from '../elements/Image';
import Modal from '../elements/Modal';
import { Link } from 'react-router-dom';
const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}

const WhatweDo = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  ...props
}) => {

  const [videoModalActive, setVideomodalactive] = useState(false);

  const openModal = (e) => {
    e.preventDefault();
    setVideomodalactive(true);
  }

  const closeModal = (e) => {
    e.preventDefault();
    setVideomodalactive(false);
  }   

  const outerClasses = classNames(
    'hero section center-content',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'hero-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container-sm">
        <div className={innerClasses}>
          <div className="hero-content">
            <h1 className="mt-0 mb-16 reveal-from-bottom" data-reveal-delay="200">
              We do  <span className="text-color-primary" style={{color: "#01A982"}}>Web Development</span>
            </h1>
            <div className="container-xs">
              <p className="m-0 mb-32 reveal-from-bottom" data-reveal-delay="400">
                We are new startup .
                </p>
              <div className="reveal-from-bottom" data-reveal-delay="600">
               {/* <ButtonGroup>
                  <Button tag="a" color="#01A982" style={{backgroundColor: "#01A982"}}  wideMobile href="https://cruip.com/">
                    Get started
                    </Button>
                  <Button tag="a" color="dark" wideMobile href="https://github.com/cruip/open-react-template/">
                    View on Github
                    </Button>
               </ButtonGroup>*/}
              </div>
            </div>
          </div>
          <div className="hero-figure reveal-from-bottom illustration-element-01" data-reveal-value="20px" data-reveal-delay="800">
                      <table class="table table-dark">
                          <thead>
                              <tr>
                                  <th scope="col">#</th>
                                  <th scope="col">app</th>
                                  <th scope="col">intrested</th>
                              </tr>
                          </thead>
                          <tbody>
                              <tr>
                                  <th scope="row">1</th>
                                  <td>Online Corporate Training</td>
                                  <td>
                                          <Link to="/event" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">2</th>
                                  <td>Personal website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">3</th>
                                  <td>Business website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">4</th>
                                  <td>Travel company website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">5</th>
                                  <td>Company website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">6</th>
                                  <td>Educational website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">7</th>
                                  <td>E-Commerce website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                              <tr>
                                  <th scope="row">8</th>
                                  <td>Online news portal website</td>
                                  <td>
                                          <Link to="/contact" > <Button tag="a" style={{ backgroundColor: "#01A982" }}>
                                              Get in Touch
                                          </Button> </Link>
                                  </td>
                              </tr>
                          </tbody>
                      </table>
          </div>
        </div>
      </div>
    </section>
  );
}

WhatweDo.propTypes = propTypes;
WhatweDo.defaultProps = defaultProps;

export default WhatweDo;