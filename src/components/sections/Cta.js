import React, { useState }from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { SectionProps } from '../../utils/SectionProps';
import Input from '../elements/Input';
import Button from '../elements/Button';
import axios from 'axios';
import BeatLoader from "react-spinners/BeatLoader";

const propTypes = {
  ...SectionProps.types,
  split: PropTypes.bool
}

const defaultProps = {
  ...SectionProps.defaults,
  split: false
}

const Cta = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  split,
  ...props
}) => {

  const [loading, setLoading] = useState(false);
  const [color, setColor] = useState("#01A982");
  const [mail,setMail] = useState('');
  const [disableSubscribe, setdisableSubscribe] = useState(true);

  const outerClasses = classNames(
    'cta section center-content-mobile reveal-from-bottom',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'cta-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider',
    split && 'cta-split'
  );
  const handleChange = (event) => {
    
    setMail(event.target.value);
    
  }
  const validateEmail = (email) => {
    if(email === "")
     return;
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if(!re.test(String(email).toLowerCase())){
      setdisableSubscribe(true);
      alert("Please provide valid Email");
    }else{
      setdisableSubscribe(false);
    }

  }



  const subscribe = () => {
    setMail("");
    //alert(mail);
    //setLoading(true);
    /*axios.post(localStorage.getItem('url') + '/contact', {
      mail:''
    })
      .then(function (response) {
        setLoading(false);
        console.log(response);
        alert("Your details got saved ! We will get back to you soon ");
      }, (error) => {
        console.log('from error')
        console.log(error);
        setLoading(false);
      })*/

      // Due to Covid Pandemic on boarding formalities would be performed virtuvally. 

    alert("subscribed !! " +mail);
  }

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container">
        <div
          className={innerClasses}
        >
          <div className="cta-slogan">
            <h3 className="m-0">
            Interested, Get in touch ?
              </h3>
          </div>
          <div className="cta-action">
            <Input id="newsletter" type="email" value={mail} onChange={handleChange} onBlur={event => validateEmail(event.currentTarget.value)} label="Subscribe" labelHidden hasIcon="right" placeholder="Your best email">
            </Input>
            
          </div>
          <Button disabled={disableSubscribe} onClick={subscribe}>subscribe</Button>
          {loading === true && <div className="sweet-loading">
                <BeatLoader color={color} loading={loading} size={30} />
              </div>
              }
        </div>
      </div>
    </section>
  );
}

Cta.propTypes = propTypes;
Cta.defaultProps = defaultProps;

export default Cta;