import React, { useRef, useEffect } from 'react';
import { useLocation, Switch } from 'react-router-dom';
import AppRoute from './utils/AppRoute';
import ScrollReveal from './utils/ScrollReveal';
import ReactGA from 'react-ga';

import Event from './Events/Event'
import Contact from './Contact/Contact'
import WhatweDo from './components/whatwedo/WhatweDo';
import Services from './components/whatwedo/Services'

// Layouts
import LayoutDefault from './layouts/LayoutDefault';

// Views 
import Home from './views/Home';

// Initialize Google Analytics okgit status

ReactGA.initialize(process.env.REACT_APP_GA_CODE);

const trackPage = page => {
  ReactGA.set({ page });
  ReactGA.pageview(page);
};

const App = () => {

  const childRef = useRef();
  let location = useLocation();

  useEffect(() => {
    const page = location.pathname;
    document.body.classList.add('is-loaded')
    childRef.current.init();
    trackPage(page);
    let localurl = "http://localhost:3000";
    let remoteUrl = "https://apptiks-api.herokuapp.com";

    localStorage.setItem('url',remoteUrl);

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  return (
    <ScrollReveal
      ref={childRef}
      children={() => (
        <Switch>
          <AppRoute exact path="/" component={Home} layout={LayoutDefault} />
          <AppRoute  path="/event" component={Event} layout={LayoutDefault} />
          <AppRoute  path="/contact" component={Contact} layout={LayoutDefault} />
          <AppRoute  path="/services" component={Services} layout={LayoutDefault} />
          
          
        </Switch>
      )} />
  );
}

export default App;