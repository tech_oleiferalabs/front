//import firebase from 'firebase-admin';
//import '@firebase/auth';
//import '@firebase/firestore';
//import * as firebase from 'firebase/app'

// Firebase App (the core Firebase SDK) is always required and must be listed first
//import firebase from "firebase/app";
// If you are using v7 or any earlier version of the JS SDK, you should import firebase using namespace import
import * as firebase from "firebase/app"

// If you enabled Analytics in your project, add the Firebase SDK for Analytics
//import "firebase/analytics";

// Add the Firebase products that you want to use
import "firebase/auth";
import "firebase/firestore";

/*const firebaseConfig = {
  apiKey: 'AIzaSyAOWHBpPhKoNhcGFKHH_Q_0AtL2gV-imgQ',
  authDomain: 'production-a9404.fireba//seapp.com',
  databaseURL: 'https://production-a9404.firebaseio.com',
  projectId: 'production-a9404',
  storageBucket: 'production-a9404.appspot.com',
  messagingSenderId: '525472070731',
  appId: '1:525472070731:web:ee873bd62c0deb7eba61ce',
};*/


const firebaseConfig = {
  apiKey: "AIzaSyBDxHIGcVCP5SM56uFFtxBdBsuDk9U5Xcw",
  authDomain: "apptiks-front.firebaseapp.com",
  projectId: "apptiks-front",
  storageBucket: "apptiks-front.appspot.com",
  messagingSenderId: "543128929072",
  appId: "1:543128929072:web:2a714cd721e0bcf8673ef5",
  measurementId: "G-BM1K7B1ZKB"
};

/*

//this is from server 
const firebaseConfig = {
  "type": "service_account",
  "project_id": "apptiks-front",
  "private_key_id": "d18c0021bdda07aab4f1c220cbf1839b0ca96483",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDKnyfunoePjT4g\n8OUCyx41uMjAgTN8ZAN9mA15H3l6guwl31Sc4JkLoje92TMNdSKzSumHXyQ5eEfo\nzglJC07yCORTUspYRG2R8LPgKPX1GX5joL7Txd34MsWmRt3RCpLbTOaZoO4U4xEV\nzUB4Z1uYIP/EbYgWuvPUu+U5OCM1av6N9pLCwIg4F1xHiej2gydkplARoYMu3f3X\netLfPriM4rkmdQAmmR/RbbS6Sx98Dudxb98Bibqc6b3BU4kby8xIkTfOuRs1Vvad\nZb5A4LfPaxPwbkbSJtt1EIj7qCDZxDJWN5MciKuVjHWDHn35GOw8GI9Y/duqIf1P\no6yzpRW/AgMBAAECggEAFZuleYgEcuWgI4ktEwAwR5BeMFFW+UP9L06g2TLnFI6T\nunI3psc46wmwohlgnWXJfo9Vmi8mQM639HbMfLKtB5i1KMGifIlHmhGuTRBxMmM2\nnE9PKmxmgIqBk3rpOtrC0IpB6U5FNYsFjmqmHnD/TpjuTbdUlP/48s3a4yA+ePqw\nP37/N3C55dvnXZAPJt5yGnsDLRQpq2v5tBFm48yPLZHSOzqQmqHWv4RR0RS/9ycr\nGk/sC+O8nsIzo/u/gI20fCNMcy9bfgshySME5oxp8II0d8Rluk2mEhqbyZbQX+Lu\nH4erG7pdnhQAQCejKyz1khD+Ujk+kwL8kQDzvfWeOQKBgQD6xxxAn5ZRSyXzYkvL\n5+BGKYFShD1cQIiyfXB2ZMbbVIHxTrm77FyPduCmSbZ/gDdt93D2jUXm6guWrgvq\nHBjgwT1FFOcVzeW6LkJxiAZsCdu7GGLXaYSPEE83rUd/pcLwpeBZJaUJMG4yfa0s\nOiH/TNk0dUFwN101+xkn1EwMiwKBgQDO11Ou5w8PYaZ1Z882HTFUuZVheQy6V1yK\n1ALalarHfpiJiPM0gaWaFYC8oe+JUGBiYfdZbMOR99jA6QhFk5AC3fah8PQ+kTaQ\nDFtN1je3+gHjAm3pk85GsEJdkdN2gTUo1mfn5qUrVaNcCHypltCC0cbw/LufDJcC\nAp6jPbo+HQKBgQC232HD4lxsdb98B088p5/NqlCrCtHwJwNIHd9YBs9Ye/yyKnWZ\nC5jH6amSuahCvA4vu8R9lrHF3SCMRelcVBb0NxZl/QMyzE3dAgTj44zDVkEM45CY\ng1LuPLd3432DIS7R7DR47dG2rMF/QlR5Lv9BvBmcfnE00rq/NZe3isHWVwKBgBtb\nZ3oOnXNPWxDUNhfWLJwkH5nQxNowi9Dg0a/Pc9MG723haylarGiAx5IraN8kflsE\n0g5F0GgR+90n9SwepEF80g4BLIzJ8AmFWN4mox/2NSGtRJRMNFAtJ9nwW6Ok2z8X\nfI6zds0QepbFmJdllxZ2YHWnN7mFlMNzTUAR/fwRAoGBAJy6ri7ZBg1I0IBihGd+\nY3mYyvSVb7aPM/o0YoywciUb44kIngJjdf+GY3csD0y0IA+hFEtQaiYzF2sALKnR\n2V09komCHBHryC64u+ZdutwbHNg9bVCseTcXvUVRhCXN3Fn8iwBd06cqGJ70M5IX\nwEl/F4rcstxw3fu03eRljbyE\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-4hldn@apptiks-front.iam.gserviceaccount.com",
  "client_id": "115983308335080035732",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-4hldn%40apptiks-front.iam.gserviceaccount.com"
};*/

/*if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}*/
//firebase.initializeApp(firebaseConfig);
//console.log('firebase config', firebase);
export default {firebase };
