import React, { useState } from 'react';
import classNames from 'classnames';
import { SectionProps } from '../utils/SectionProps';
import ButtonGroup from '../components/elements/ButtonGroup';
import Button from '../components/elements/Button';
import Image from '../components/elements/Image';
import Modal from '../components/elements/Modal';
import { Link } from 'react-router-dom';
import IntrestModal from '../components/elements/IntrestModal';
import { useFormik } from 'formik';

const propTypes = {
  ...SectionProps.types
}

const defaultProps = {
  ...SectionProps.defaults
}




const Event = ({
  className,
  topOuterDivider,
  bottomOuterDivider,
  topDivider,
  bottomDivider,
  hasBgColor,
  invertColor,
  ...props
}) => {

  const [intrestModalActive, setIntrestModalActive] = useState(false);

  const [videoModalActive, setVideomodalactive] = useState(false);

  const openModal = (e) => {
    e.preventDefault();
    setVideomodalactive(true);
  }

  const closeModal = (e) => {
    e.preventDefault();
    setVideomodalactive(false);
  }   

  const outerClasses = classNames(
    'hero section center-content',
    topOuterDivider && 'has-top-divider',
    bottomOuterDivider && 'has-bottom-divider',
    hasBgColor && 'has-bg-color',
    invertColor && 'invert-color',
    className
  );

  const innerClasses = classNames(
    'hero-inner section-inner',
    topDivider && 'has-top-divider',
    bottomDivider && 'has-bottom-divider'
  );

  const intrested = () =>{
    //alert("intrested !!");
    //setIntrestModalActive(true);
  }

  const register = () =>{

  }

  return (
    <section
      {...props}
      className={outerClasses}
    >
      <div className="container-sm">
        <div className={innerClasses}>
          <div className="hero-content">
            <h1 className="mt-0 mb-16 reveal-from-bottom" data-reveal-delay="200">
              Online Corporate Training for Limited <span className="text-color-primary">Folks</span>
            </h1>
            <div className="container-sm">
              <p className="m-0 mb-32 reveal-from-bottom" data-reveal-delay="400">
                    Html, javascript / java are pre requisites for below courses
                </p>
              <div className="reveal-from-bottom" data-reveal-delay="600">
              <table class="table table-dark">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Course</th>
      <th scope="col">Technologies</th>
      <th scope="col">Duration</th>
      <th scope="col">How do you like it ?</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">1</th>
      <td>React-Express-Firebae</td>
      <td>React Node js Express js Firebase</td>
      <td>45 days</td>
      <td>
      <ButtonGroup>
      <Link to="/contact" > <Button tag="a" style={{backgroundColor: "#5658DD"}} onClick={intrested}>
                    Intrested
                    </Button> </Link>
                   
      </ButtonGroup>
      </td>
    </tr>
    <tr>
      <th scope="row">2</th>
      <td>Angular-Express-Firebae</td>
      <td>Angular Node js Express js Firebase</td>
      <td>45 days</td>
      <td>
      <ButtonGroup>
                  <Button tag="a" style={{backgroundColor: "#5658DD"}}  onClick={intrested}>
                    Intrested
                    </Button>
                   
      </ButtonGroup>
      </td>
    </tr>
    <tr>
      <th scope="row">3</th>
      <td>React-Springboot-Firebae</td>
      <td>React Spring boot Firebase</td>
      <td>45 days</td>
      <td>
      <ButtonGroup>
                  <Button tag="a" style={{backgroundColor: "#5658DD"}} onClick={intrested}>
                    Intrested
                    </Button>
                   
      </ButtonGroup>
      </td>
    </tr>


    <tr>
      <th scope="row">4</th>
      <td>Angular-Springboot-Firebae</td>
      <td>Angular Spring boot Firebase</td>
      <td>45 days</td>
      <td>
      <ButtonGroup>
                  <Button tag="a" style={{backgroundColor: "#5658DD"}} onClick={intrested}>
                    Intrested
                    </Button>
      </ButtonGroup>
      </td>
    </tr>


  </tbody>
</table>
              </div>
            </div>
          </div>
          <div className="hero-figure reveal-from-bottom illustration-element-01" data-reveal-value="20px" data-reveal-delay="800">
            <a
              data-video="https://player.vimeo.com/video/174002812"
              href="#0"
              aria-controls="video-modal"
              onClick={openModal}
            >
             
            </a>
          </div>


{intrestModalActive && <IntrestModal/>}
<IntrestModal/>
          <Modal
            id="video-modal"
            show={videoModalActive}
            handleClose={closeModal}
            video="https://player.vimeo.com/video/174002812"
            videoTag="iframe" />
        </div>
      </div>
    </section>
  );
}

Event.propTypes = propTypes;
Event.defaultProps = defaultProps;

export default Event;