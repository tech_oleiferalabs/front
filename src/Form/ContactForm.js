import React, { useState } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import   './contactform.css';
import axios from 'axios';
import BeatLoader from "react-spinners/BeatLoader";
const SignupSchema = Yup.object().shape({
  name: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  subject: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Subject is equired'),
  email: Yup.string().email('Invalid email').required('Email is Required'),
  phone:Yup.string().required("Phone is Required")
});

 function  ContactForm  (props) {
  const [loading, setLoading] = useState(false);
  const [color, setColor] = useState("#01A982");
 return (
  <div>
    <Formik
      initialValues={{
        name: '',
        phone: '',
        email: '',
        subject:'',
        more:''
      }}
      validationSchema={SignupSchema}
      onSubmit={(values,{resetForm}) => {
        // same shape as initial values
        console.log(values);
       
          setLoading(true);
          axios.post(localStorage.getItem('url') + '/contact', values)
            .then(function (response) {
              setLoading(false);
              console.log(response);
              alert("Your details got saved ! We will get back to you soon ");
              resetForm();
            }, (error) => {
              console.log('from error')
              console.log(error);
              setLoading(false);
            })

            
      }}
    >
      {({ errors, touched }) => (
        <Form>
            <div>
                     <div>
                         <div style={{marginRight:'180px',padding:'5px'}}>Name</div>
                         <div><Field name="name" className="border-2 border-red-500"/></div>
                         <div>
                             {errors.name && touched.name ? (
                                 <div>{errors.name}</div>
                             ) : null}
                         </div>
                     </div>

                     <div>
                         <div style={{marginRight:'180px',padding:'5px'}}>Phone</div>
                         <div><Field name="phone" /></div>
                         <div>
                             {errors.phone && touched.phone ? (
                                 <div>{errors.phone}</div>
                             ) : null}
                         </div>
                     </div>

                     <div>
                         <div style={{marginRight:'180px',padding:'5px'}}>Email</div>
                         <div><Field name="email" /></div>
                         <div>
                             {errors.email && touched.email ? (
                                 <div>{errors.email}</div>
                             ) : null}
                         </div>
                     </div>

                     <div>
                         <div style={{marginRight:'165px',padding:'5px'}}>Subject</div>
                         <div><Field name="subject" /></div>
                         <div>
                             {errors.subject && touched.subject ? (
                                 <div>{errors.subject}</div>
                             ) : null}
                         </div>
                     </div>

                     <div>
                         <div style={{marginRight:'150px',padding:'5px'}}>More info</div>
                         <div><Field name="more" /></div>
                         <div>
                             {errors.more && touched.more ? (
                                 <div>{errors.more}</div>
                             ) : null}
                         </div>
                     </div>
                     <div style={{padding:'20px'}}>
                        <button style={{backgroundColor: "#5658DD"}}  className="button btnPrimary button-wide-mobile button-sm" type="submit">Submit</button>
                    </div>
                   
          </div>
        </Form>
        
      )}
    </Formik>
            {loading === true && <div className="sweet-loading">
                <BeatLoader color={color} loading={loading} size={30} />
              </div>
              }

  </div>
);
                             }
export default ContactForm;